var env = process.env.NODE_ENV || 'development';
var config = require('../config/config')[env];

//inicializar sequelize
var Sequelize = require('Sequelize');
var sequelize = new Sequelize(config.db);

//entidade Filme:
var Filme = sequelize.define('Filme', {
    filme : {
        type: Sequelize.STRING,
        unique : true,
        validate : {
            notEmpty : true,
        }
    },
    rating : {
        type : Sequelize.DOUBLE,
        allowNull : false,
        validate : {
            min : 0,
            max : 10
        }
    },
    descricao : {
        type : Sequelize.STRING(2048),
        allowNull : false
    },
    lancamento : {
        type : Sequelize.DATE
    },
    LinkPosterImg : Sequelize.STRING(500),
    LinkBackgroundImg : Sequelize.STRING(500)
}, {
    updatedAt : false
});

var ApiKey = sequelize.define('ApiKey', {
    apikey : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV4,
        unique : true,
        allowNull : false
    }
}, {updatedAt : false});

//para exportar
var database = {};
database.Filme = Filme;
database.ApiKey = ApiKey;
database.sequelize = sequelize;
database.Sequelize = Sequelize;

//returns!
module.exports = database;
