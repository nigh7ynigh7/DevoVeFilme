# DevoVeFilme : SOBRE

Projeto que foi desenvolvido permite realizar cadastros de filmes e seriados com uma nota qualquer. Dependendo da nota, o consumidor vai falar se pode ou não ver io filme.

O cliente que cria registros foi construido usando .NET.

O cliente que visualizará o conteúdo foi construido com HTML + Angular.js

O serviço REST criado usando NODE.JS com o módulo HTTPS

E é basicamente isso.

## Tema

O tema deste projeto é criar um API para desenvolver um sistema para pessoas que não consegue decidir se vale a pena ou não ver um filme. Daí, este projeto.

# Requerimentos do trabalho

* ~~O grupo deverá desenvolver um serviço web REST que possibilite adicionar e consultar itens~~
    * ~~o grupo deverá definir um tema, desde que contenha no mínimo quatro atributos~~
    * ~~Deverá ser utilizado a linguagem JAVA ou .NET ou PHP. Node.js foi aceitável.~~
* ~~O serviço poderá armazenar os itens em qualquer local, desde armazenamento em memória até em um banco de dados~~
    * ~~1 ponto extra para o grupo que desenvolver com banco de dados.~~
* ~~Deverá ser desenvolvido duas aplicações clientes:~~
    * ~~sendo um cliente Android ou IOS ou Windows Forms (a linguagem deverá ser diferente da linguagem utilizada na criação do serviço)~~
    * ~~e outro cliente utilizando o Angular JS.~~
    * ~~Sendo que um cliente será responsável por adicionar os itens e o outro por consultar~~.
* ~~Cada grupo poderá ter no máximo 2 pessoas.~~

# Arquitetura

A arquitetura a ser adota é um RESTful BCM.

A fronteira foi feito com o módulo `express`. O banco de dados é gerenciado com o módulo `Sequelize.js`, um ORM para node.

# O API

## Get

* `/api/busca` Com uma entrada qualquer de texto `q`, o API traz uma lista de filmes que foi cadastrado contendo esse texto.
    * Você deve apenas informar um string para busca.
* `/api/recente` Tras uma lista dos 25 filmes que foram recentemente adicionados.
    * Você não precisa informar nada
* `/api/filme` Traz todos os filmes gravado no banco de dados.
* `/api/filme/:filme` A partir de um nome, o sistema buscará os dados contidos no sistema relacionado com um filme.
    * Só o nome do filme heh.

## Post

* `/api/filme/` Voce criará um filme com isso. *Necessita-se de uma chave API com a classe Filme*.
    * você deve enviar um entidade `Filme` juntamente com uma chave para acessar o API.


## Delete

* `/api/filme/:id` Delete um filme segundo seu id. *Necessita-se de uma chave API com a classe Filme*.

# Entidades

Se o cliente não utiliza JS como padrão, seria necessário desenvolver as seguintes entidade:

## ReqMsg

```
class RequestMessage <T>
    T      input
    string key   // api key
```

## RtnMsg

```
class ReturnMessage
    bool   success
    string msg
    string objJson

```

## Movie

```
class Filme
    string filme
    double rating
    string descricao
    datetime lancamento
    string LinkPosterImg
    string LinkBakgroundImg
```

# O Banco de dados

Conterá apenas duas tabela com as seguintes especificações:

```
Filme
  ┬
  ├── serial     id
  ├── varchar    filme NN
  ├── double     rating NN (check <= 10, check >= 0)
  ├── varchar    descricao NN
  ├── timestamp  lancamento
  ├── varchar    LinkPosterImg
  └── varchar    LinkBakgroundImg

ApiKey
  ┬
  └── uuid    apikey

```

Desenvolvido com PostgresSQL.

Note que essas restrições serão aplicadas ao realizar o cadastro (check e not null).

Cadastro só pode ser feito se voce possui uma chave (API)

# Executar o projeto

Para executar o servidor, algumas coisas deve ser feitas antemão.

## Instalar PostgresSQL

PostgresSQL é o banco de dados que é utilizando durante a realização desse projeto. É o banco de dados principal. Então, você vai precisar de instalar esse banco antes de qualquer outra coisa.

Então, para instalar postgres, basta baixar esse instalador: `http://www.enterprisedb.com/products-services-training/pgdownload#windows`

## Criar Banco

Você deve criar o banco. Só um banco vazio com o nome certo dá.

1. Abre pgAdmin III
2. Faça login no servidor `PostgreSQL 9.x`
3. Cria novo banco com nome `Devovefilme`

NOTE: NOME É `Devovefilme`, NÃO ESQUECE

## Intalar node

Se você quiser instalar Node para executar esse projeto e para realizar futuros projetos.

1. Baixa e instala um desses dois instaladores: `https://nodejs.org/dist/v4.2.2/node-v4.2.2-x64.msi` (x64) ou `https://nodejs.org/dist/v4.2.2/node-v4.2.2-x86.msi` (x86)

## Executar o projeto.

Caso for a primeira vez executando o projeto, executa essas linhas de comando (windows):

```
cd "C:\VAI ATÉ O DITRETÓRIO DO PROJETO\"
npm install --save
```

Se você já tem os módulos instalados (descritos no package.json), executa isso no consle:

```
node server.js
```

## E agora?

Com o servidor rodando, você pode abrir `index.html` contido na pasta `/lientes/angular/` no seu navegador e você poderá ver a parte de angular.js funcionando direitinho.

### Não tem nenhum filme aqui!

É claro! Você tem que colocar filmes no banco de dados. Como você faz isso.

Executa isso no pgAdmin `/extra/insersao do filme.sql` no banco de dados que você criou uns passos atrás - `Devovefilme`.

Ou, você pode fazer o seguinte:

Abrir o projeto contuido na pasta `/clientes/net/` no Visual Studio e executar ele. Agora é só rodar o projeto e feliz seja você - vai cadastrar novos filmes.
