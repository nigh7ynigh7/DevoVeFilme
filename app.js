var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var config = require('./config/config')[env];
var util = require('util');

var entity = require('./sequelize/');

var model = require('./model/')(entity);

var express = require('express');
var app = express();

app.use(require('body-parser')());

//deixar qualquer tipo de acesso
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/api/busca',function(req, res){
    console.log("GET /api/busca");
    var query = req.query.q;
    if (query.length === 0) return res.json({success : false, mgs : 'Você precisa de informar o nome do filme'});
    var q = model.Filme.find(query);
    q.then(function(a){
        return res.json({success : true, msg : "Filmes que foram encontrados", objJson : JSON.stringify(a)});
    }, function(a){
        return res.json({success : false, msg : "Erro desconhecido"});
    });
});

app.get('/api/recente',function(req, res){
    console.log("GET /api/recente");
    var q = model.Filme.recent();
    q.then(function(a){
        return res.json({success : true, msg : "Filmes que foram lançados a pouco tempo", objJson : JSON.stringify(a)});
    }, function(a){
        return res.json({success : false, msg : "Erro desconhecido"});
    });
});

app.get('/api/filme',function(req, res){
    console.log("GET /api/filme");
    var q = model.Filme.all();
    q.then(function(a){
        return res.json({success : true, msg : "Filmes que foram lançados a pouco tempo", objJson : JSON.stringify(a)});
    }, function(a){
        return res.json({success : false, msg : "Erro desconhecido"});
    });
});

app.get('/api/filme/:nome',function(req, res){
    console.log("GET /api/filme/:nome");
    var name = req.params.nome;

    if (name.length === 0) return res.json({success : false, mgs : 'Você precisa de informar o nome do filme'});

    var q = model.Filme.findOne(name);

    q.then(function(a){
        return res.json({success:true, msg:"Filme encontrado", objJson: JSON.stringify(a)});
    },function(a){
        return res.json({success:false, msg:"Filme não foi encontrado"});
    });
});

app.post('/api/filme',function(req, res){
    console.log("POST /api/filme");
    var filme = req.body.input,
        api   = req.body.key;
    if (!filme || !api) return res.json({success : false, mgs : 'Você precisa de informar o filme e o API'});
    var allow = model.API.allow(api);

    allow.then(function(a){
        if(!a) return this.reject();
        if (util.isString(filme))
            filme = JSON.parse(filme);
        console.log(filme.filme, filme.rating, filme.descricao, filme.lancamento, filme.LinkPosterImg, filme.LinkBackgroundImg);
        return model.Filme.create(filme.filme, filme.rating, filme.descricao, filme.lancamento, filme.LinkPosterImg, filme.LinkBackgroundImg);
    }).then(function(a){
        return res.json({success:true, msg:"Filme criado com sucesso", objJson: JSON.stringify(a)});
    },function(a){
        return res.json({success:false, msg:"Filme não foi criado"});
    });
});

app.delete('/api/filme/:id',function(req, res){
    console.log("DELETE /api/filme/#");
    var id    = req.params.id,
        api   = req.query.key;
    if (!api || !id) return res.json({success : false, mgs : 'Você precisa de informar o id do filme e o API'});

    var allow = model.API.allow(api);

    allow.then(function(a){
        if(!a) return this.reject();
        return model.Filme.destroy(id);
    }).then(function(a){
        return res.json({success:true, msg:"Filme deletado com sucesso", objJson: JSON.stringify(a)});
    },function(a){
        return res.json({success:false, msg:"Filme não foi deletado"});
    });
});

module.exports.start = function(){
    entity.sequelize.sync(/*{force:true}*/).then(function(){
        app.listen(config.port);

        console.log('Listenin@'+config.port);
    });
};
