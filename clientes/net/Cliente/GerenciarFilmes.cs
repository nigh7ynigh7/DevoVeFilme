﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Cliente
{
    public partial class GerenciarFilmes : Form
    {
        private double regVal = 0;

        public GerenciarFilmes()
        {
            InitializeComponent();

            GetTodosFilmes();
        }

        private async void GetTodosFilmes()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2525/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //GET
                HttpResponseMessage response = await client.GetAsync("api/filme");
                if (response.IsSuccessStatusCode)
                {
                    List<Filme> listaFilme = null;
                    ReturnMessage filme = await response.Content.ReadAsAsync<ReturnMessage>();
                    if (filme.success)
                    {
                        listaFilme = JsonConvert.DeserializeObject<List<Filme>>(filme.objJson);
                    }

                    DataTable filmes = new DataTable("Filmes");
                    filmes.Columns.Add(new DataColumn("Filme"));
                    filmes.Columns.Add(new DataColumn("Avaliação"));
                    filmes.Columns.Add(new DataColumn("Lançamento"));

                    foreach (var i in listaFilme)
                    {
                        DataRow dr;
                        dr = filmes.NewRow();
                        dr["Filme"] = i.filme;
                        dr["Avaliação"] = i.rating;
                        dr["Lançamento"] = i.lancamento;
                        filmes.Rows.Add(dr);
                    }

                    dgvFilmes.DataSource = filmes;
                }
            }
        }

        private Filme GetFilme()
        {
            Filme filme = new Filme();

            filme.filme = txtNomeFilme.Text;
            filme.rating = regVal;
            filme.descricao = txtDescricao.Text;
            filme.lancamento = dtpLancamento.Value;
            filme.LinkPosterImg = txtLinkPoster.Text;
            filme.LinkBackgroundImg = txtLinkFundo.Text;

            return filme;
        }

        private async void CadastrarFilme()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:2525/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                var novoFilme = GetFilme();
                var reqFilme = new RequestMessage<Filme>();
                reqFilme.key = "b75894b6-fa42-44f9-84d8-569f812aebf3";
                reqFilme.input = novoFilme;

                var response = await client.PostAsJsonAsync("/api/filme", reqFilme);
                if (response.IsSuccessStatusCode)
                {
                    Uri novoFilmeUrl = response.Headers.Location;
                }
            }

            LimpaForm();
            GetTodosFilmes();
        }

        private void LimpaForm()
        {
            txtNomeFilme.Text = "";
            txtDescricao.Text = "";
            dtpLancamento.Value = DateTime.Now;
            txtLinkPoster.Text = "";
            txtLinkFundo.Text = "";
            tkbAvaliacao.Value = 0;
            lblAvaliacaoTkb.Text = "0";
            regVal = 0;
        }

        private void tkbAvaliacao_Scroll(object sender, EventArgs e)
        {
            //lblAvaliacaoTkb.Text = (Convert.ToDouble(tkbAvaliacao.Value.ToString())/10).ToString();
            //regVal = Double.Parse(lblAvaliacaoTkb.Text);
            regVal = tkbAvaliacao.Value;
            regVal /= 10;
            lblAvaliacaoTkb.Text = regVal.ToString();

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            CadastrarFilme();
        }
    }
}
