﻿namespace Cliente
{
    partial class GerenciarFilmes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbCadastrarFilme = new System.Windows.Forms.GroupBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.txtLinkFundo = new System.Windows.Forms.TextBox();
            this.lblLinkFundo = new System.Windows.Forms.Label();
            this.txtLinkPoster = new System.Windows.Forms.TextBox();
            this.lblLinkPoster = new System.Windows.Forms.Label();
            this.dtpLancamento = new System.Windows.Forms.DateTimePicker();
            this.lblLancamento = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.lblAvaliacaoTkb = new System.Windows.Forms.Label();
            this.tkbAvaliacao = new System.Windows.Forms.TrackBar();
            this.lblAvaliacao = new System.Windows.Forms.Label();
            this.lblNomeFilme = new System.Windows.Forms.Label();
            this.txtNomeFilme = new System.Windows.Forms.TextBox();
            this.dgvFilmes = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.gpbFilmes = new System.Windows.Forms.GroupBox();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.gpbCadastrarFilme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tkbAvaliacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilmes)).BeginInit();
            this.gpbFilmes.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbCadastrarFilme
            // 
            this.gpbCadastrarFilme.Controls.Add(this.btnCadastrar);
            this.gpbCadastrarFilme.Controls.Add(this.txtLinkFundo);
            this.gpbCadastrarFilme.Controls.Add(this.lblLinkFundo);
            this.gpbCadastrarFilme.Controls.Add(this.txtLinkPoster);
            this.gpbCadastrarFilme.Controls.Add(this.lblLinkPoster);
            this.gpbCadastrarFilme.Controls.Add(this.dtpLancamento);
            this.gpbCadastrarFilme.Controls.Add(this.lblLancamento);
            this.gpbCadastrarFilme.Controls.Add(this.txtDescricao);
            this.gpbCadastrarFilme.Controls.Add(this.lblDescricao);
            this.gpbCadastrarFilme.Controls.Add(this.lblAvaliacaoTkb);
            this.gpbCadastrarFilme.Controls.Add(this.tkbAvaliacao);
            this.gpbCadastrarFilme.Controls.Add(this.lblAvaliacao);
            this.gpbCadastrarFilme.Controls.Add(this.lblNomeFilme);
            this.gpbCadastrarFilme.Controls.Add(this.txtNomeFilme);
            this.gpbCadastrarFilme.Location = new System.Drawing.Point(19, 24);
            this.gpbCadastrarFilme.Name = "gpbCadastrarFilme";
            this.gpbCadastrarFilme.Size = new System.Drawing.Size(365, 386);
            this.gpbCadastrarFilme.TabIndex = 0;
            this.gpbCadastrarFilme.TabStop = false;
            this.gpbCadastrarFilme.Text = "Cadastrar Filme";
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Location = new System.Drawing.Point(275, 347);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(75, 23);
            this.btnCadastrar.TabIndex = 4;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // txtLinkFundo
            // 
            this.txtLinkFundo.Location = new System.Drawing.Point(100, 314);
            this.txtLinkFundo.Name = "txtLinkFundo";
            this.txtLinkFundo.Size = new System.Drawing.Size(250, 20);
            this.txtLinkFundo.TabIndex = 12;
            // 
            // lblLinkFundo
            // 
            this.lblLinkFundo.AutoSize = true;
            this.lblLinkFundo.Location = new System.Drawing.Point(16, 318);
            this.lblLinkFundo.Name = "lblLinkFundo";
            this.lblLinkFundo.Size = new System.Drawing.Size(78, 13);
            this.lblLinkFundo.TabIndex = 11;
            this.lblLinkFundo.Text = "Link do Fundo:";
            // 
            // txtLinkPoster
            // 
            this.txtLinkPoster.Location = new System.Drawing.Point(100, 281);
            this.txtLinkPoster.Name = "txtLinkPoster";
            this.txtLinkPoster.Size = new System.Drawing.Size(250, 20);
            this.txtLinkPoster.TabIndex = 10;
            // 
            // lblLinkPoster
            // 
            this.lblLinkPoster.AutoSize = true;
            this.lblLinkPoster.Location = new System.Drawing.Point(16, 285);
            this.lblLinkPoster.Name = "lblLinkPoster";
            this.lblLinkPoster.Size = new System.Drawing.Size(78, 13);
            this.lblLinkPoster.TabIndex = 9;
            this.lblLinkPoster.Text = "Link do Poster:";
            // 
            // dtpLancamento
            // 
            this.dtpLancamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLancamento.Location = new System.Drawing.Point(100, 249);
            this.dtpLancamento.Name = "dtpLancamento";
            this.dtpLancamento.Size = new System.Drawing.Size(250, 20);
            this.dtpLancamento.TabIndex = 8;
            // 
            // lblLancamento
            // 
            this.lblLancamento.AutoSize = true;
            this.lblLancamento.Location = new System.Drawing.Point(25, 253);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.Size = new System.Drawing.Size(69, 13);
            this.lblLancamento.TabIndex = 7;
            this.lblLancamento.Text = "Lançamento:";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(100, 96);
            this.txtDescricao.Multiline = true;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(250, 140);
            this.txtDescricao.TabIndex = 6;
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(36, 99);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(58, 13);
            this.lblDescricao.TabIndex = 5;
            this.lblDescricao.Text = "Descrição:";
            // 
            // lblAvaliacaoTkb
            // 
            this.lblAvaliacaoTkb.AutoSize = true;
            this.lblAvaliacaoTkb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvaliacaoTkb.Location = new System.Drawing.Point(332, 64);
            this.lblAvaliacaoTkb.Name = "lblAvaliacaoTkb";
            this.lblAvaliacaoTkb.Size = new System.Drawing.Size(18, 20);
            this.lblAvaliacaoTkb.TabIndex = 4;
            this.lblAvaliacaoTkb.Text = "0";
            // 
            // tkbAvaliacao
            // 
            this.tkbAvaliacao.Location = new System.Drawing.Point(92, 56);
            this.tkbAvaliacao.Maximum = 100;
            this.tkbAvaliacao.Name = "tkbAvaliacao";
            this.tkbAvaliacao.Size = new System.Drawing.Size(236, 45);
            this.tkbAvaliacao.TabIndex = 3;
            this.tkbAvaliacao.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tkbAvaliacao.Scroll += new System.EventHandler(this.tkbAvaliacao_Scroll);
            // 
            // lblAvaliacao
            // 
            this.lblAvaliacao.AutoSize = true;
            this.lblAvaliacao.Location = new System.Drawing.Point(37, 64);
            this.lblAvaliacao.Name = "lblAvaliacao";
            this.lblAvaliacao.Size = new System.Drawing.Size(57, 13);
            this.lblAvaliacao.TabIndex = 2;
            this.lblAvaliacao.Text = "Avaliação:";
            // 
            // lblNomeFilme
            // 
            this.lblNomeFilme.AutoSize = true;
            this.lblNomeFilme.Location = new System.Drawing.Point(14, 31);
            this.lblNomeFilme.Name = "lblNomeFilme";
            this.lblNomeFilme.Size = new System.Drawing.Size(80, 13);
            this.lblNomeFilme.TabIndex = 1;
            this.lblNomeFilme.Text = "Nome do Filme:";
            // 
            // txtNomeFilme
            // 
            this.txtNomeFilme.Location = new System.Drawing.Point(100, 27);
            this.txtNomeFilme.Name = "txtNomeFilme";
            this.txtNomeFilme.Size = new System.Drawing.Size(250, 20);
            this.txtNomeFilme.TabIndex = 0;
            // 
            // dgvFilmes
            // 
            this.dgvFilmes.AllowUserToAddRows = false;
            this.dgvFilmes.AllowUserToDeleteRows = false;
            this.dgvFilmes.AllowUserToResizeColumns = false;
            this.dgvFilmes.AllowUserToResizeRows = false;
            this.dgvFilmes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilmes.Location = new System.Drawing.Point(16, 62);
            this.dgvFilmes.MultiSelect = false;
            this.dgvFilmes.Name = "dgvFilmes";
            this.dgvFilmes.ReadOnly = true;
            this.dgvFilmes.RowHeadersVisible = false;
            this.dgvFilmes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFilmes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvFilmes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFilmes.Size = new System.Drawing.Size(332, 270);
            this.dgvFilmes.TabIndex = 1;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(273, 25);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // gpbFilmes
            // 
            this.gpbFilmes.Controls.Add(this.btnExcluir);
            this.gpbFilmes.Controls.Add(this.txtBuscar);
            this.gpbFilmes.Controls.Add(this.dgvFilmes);
            this.gpbFilmes.Controls.Add(this.btnBuscar);
            this.gpbFilmes.Location = new System.Drawing.Point(400, 24);
            this.gpbFilmes.Name = "gpbFilmes";
            this.gpbFilmes.Size = new System.Drawing.Size(365, 386);
            this.gpbFilmes.TabIndex = 3;
            this.gpbFilmes.TabStop = false;
            this.gpbFilmes.Text = "Filmes Cadastrados";
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(226, 347);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(122, 23);
            this.btnExcluir.TabIndex = 13;
            this.btnExcluir.Text = "Excluir Selecionado";
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(16, 27);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(250, 20);
            this.txtBuscar.TabIndex = 3;
            // 
            // GerenciarFilmes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 431);
            this.Controls.Add(this.gpbFilmes);
            this.Controls.Add(this.gpbCadastrarFilme);
            this.MaximizeBox = false;
            this.Name = "GerenciarFilmes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DevoVeFilme - Gerenciar Filmes";
            this.gpbCadastrarFilme.ResumeLayout(false);
            this.gpbCadastrarFilme.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tkbAvaliacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilmes)).EndInit();
            this.gpbFilmes.ResumeLayout(false);
            this.gpbFilmes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbCadastrarFilme;
        private System.Windows.Forms.Label lblAvaliacaoTkb;
        private System.Windows.Forms.TrackBar tkbAvaliacao;
        private System.Windows.Forms.Label lblAvaliacao;
        private System.Windows.Forms.Label lblNomeFilme;
        private System.Windows.Forms.TextBox txtNomeFilme;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.TextBox txtLinkFundo;
        private System.Windows.Forms.Label lblLinkFundo;
        private System.Windows.Forms.TextBox txtLinkPoster;
        private System.Windows.Forms.Label lblLinkPoster;
        private System.Windows.Forms.DateTimePicker dtpLancamento;
        private System.Windows.Forms.Label lblLancamento;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.DataGridView dgvFilmes;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox gpbFilmes;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.TextBox txtBuscar;
    }
}

