var local = "http://localhost:2525/api/";

app.controller('Principal', ['$http', '$scope', function($http, $scope){
    $scope.filmes = [];
    $scope.buscados = [];

    $scope.filmeAtual = null;

    $scope.mode = 'index';

    var receberFilme = function(){
        $http.get(local+'recente').then(function(a){
            console.log(a);
            if (a.data.success) {
                $scope.filmes = JSON.parse(a.data.objJson);
            }else {
                return toastr.warning('Houve um erro ao retornar uma lista de filmes recentes.', 'Desculpe!');
            }
        },function(a){
            return toastr.warning('Houve um erro ao conectar com o API. Infelizmente, nenhum filme recente foi retornado.', 'Erro de conexão');
        });
    };
    receberFilme();
    var interval = setInterval(receberFilme, 300000);

    this.selecionarFilme = function(filme){
        $scope.filmeAtual = filme;
        $scope.filmeAtual.lancamento = new Date($scope.filmeAtual.lancamento);

        var x = $(window).width(),
            y = $(window).height(),
            bannerY = Math.floor(y*0.6);

        $('#filme-header').css('background-image','url(' + filme.LinkBackgroundImg + ')');
        $('#filme-header').css('background-position','center');
        $('#filme-header').css('filter','blur(4px)');
        $('#filme-header').css('transform','translateY(-7px)');
        $('#filme-header').css('height', bannerY);
        $('#gradient').css('height', bannerY);
        $('#conteudo').css('transform', 'translateY(-'+(Math.floor(bannerY*1.30/2))+'px)');

        if (filme.rating < 6) {
            $('#aceitacao').css('background-color','rgb(252, 91, 56)');
            $('#aceitacao').css('color','rgb(252, 250, 232)');
            $('#aceitacao').html('<h4 class="text-center">NÃO</h4>');
        }else if (filme.rating < 7) {
            $('#aceitacao').css('background-color','rgb(247, 181, 11)');
            $('#aceitacao').css('color','rgb(138, 74, 0)');
            $('#aceitacao').html('<h4 class="text-center">TALVEZ</h4>');
        }else if (filme.rating < 8) {
            $('#aceitacao').css('background-color','rgb(131, 221, 1)');
            $('#aceitacao').css('color','rgb(88, 115, 0)');
            $('#aceitacao').html('<h4 class="text-center">COM CERTEZA</h4>');
        }else if (filme.rating >= 8) {
            $('#aceitacao').css('background-color','rgb(0, 205, 64)');
            $('#aceitacao').css('color','rgb(0, 77, 49)');
            $('#aceitacao').html('<h4 class="text-center">DEFINITIVAMENTE SIM</h4>');
        }
        $('#index').hide();
        $('#busca').hide();
        $scope.buscados = [];
        $('#filme').fadeIn(1500);
        $(window).scrollTo('0px', 0);
    };

    this.vaiParaIndex = function(){
        $('#filme').hide();
        $('#busca').hide();
        $scope.buscados = [];
        $scope.filmeAtual = null;
        $('#index').fadeIn(1500);
    };
    var vaiParaBusca = function(){
        $('#filme').hide();
        $('#index').hide();
        $('#busca').hide();
        $scope.filmeAtual = null;
        $('#busca').fadeIn(1000);
    };

    $scope.mainBusca = '';
    $scope.filmeBusca = '';
    $scope.currentQuery = '';

    this.buscar = function(query){
        console.log(query);
        if (!query)
            return toastr.info('Você tem que digitar algo na caixinha de texto, ao buscar, ok?', 'Faltou algo...');
        $scope.currentQuery = query;
        $http.get(local+'busca?q='+query).then(function(a){
            console.log(a);
            if (a.data.success) {
                $scope.buscados = JSON.parse(a.data.objJson);
                vaiParaBusca();
            }else {
                return toastr.warning('Houve um erro ao retornar uma lista de filmes recentes.', 'Desculpe!');
            }
        },function(a){
            return toastr.warning('Houve um erro ao conectar com o API. Infelizmente, nenhum resultado da busca foi retornado.', 'Erro de conexão');
        });
    };
}]);
