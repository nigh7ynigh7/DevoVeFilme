$('#noJS').hide();
$('#JS').fadeIn(500);

//calcular tamanho da tela
var y = $(window).height();
var x = $(window).width();

$('#bg').height(Math.floor(y*0.90));
$('#content').height(Math.floor(y*0.90));

var centerSet = ($('#content').height()/2) - ($('#well').height()/2);

$('#well').css('margin-top',Math.floor(centerSet));

$('#baixo').height(y - Math.floor(y*0.90));
$('#arrow').css('margin-top',
    Math.floor(
        (y*0.1/2) - ($('#arrow').height()/2)
    ));
$('#recente').css('min-height',Math.floor(y*0.75)+'px');


$('#arrow').click(function(){
    $(window).scrollTo(
        $('#recente'), 500);
    // $(window).scrollTo($('#recente'), {axis:'y' , duration:800});
});
$('#arrowUp').click(function(){
    $(window).scrollTo(0, 750);
});


$('#filme').hide();
$('#busca').hide();
