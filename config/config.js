var path = require('path');

var config = {
    "production" : {
        db : null,
        root : path.join(__dirname,'/../../'),
        port : process.env.PORT || 80
    },
    "development" : {
        db : "postgres://postgres:postgres@localhost:1337/Devovefilme",
        root : path.join(__dirname,'/../../'),
        port : process.env.PORT || 2525
    }
};

module.exports = config;
