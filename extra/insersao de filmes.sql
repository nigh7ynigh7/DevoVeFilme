insert into
    "Filmes"
        (
            filme,
            rating,
            descricao,
            lancamento,
            "LinkPosterImg",
            "LinkBackgroundImg",
            "createdAt"
        )

    values
        (
            'Detona Ralph',
            7.8,
            'Ralph (John C. Reilly) é o vilão de Conserta Félix Jr., um popular jogo de fliperama que está completando 30 anos. Apesar de cumprir suas tarefas à perfeição, Ralph gostaria de receber uma atenção maior de Felix Jr. (Jack McBrayer) e os demais habitantes do jogo, que nunca o convidam para festas e nem mesmo o tratam bem. Para provar que merece tamanha atenção, ele promete que voltará ao jogo com uma medalha de herói no peito, no intuito de mostrar seu valor. É o início da peregrinação de Ralph por outros jogos, em busca de um meio de obter sua sonhada medalha. ',
            '2013-1-4',
            'https://upload.wikimedia.org/wikipedia/en/1/15/Wreckitralphposter.jpeg?1447852721443',
            'http://www.holyblasphemy.net/wp-content/uploads/2014/01/Wreck-It-Ralph.jpg',
            now()
        ),
        (
            'Divertidamente',
            8.4,
            'Riley é uma garota divertida de 11 anos de idade, que deve enfrentar mudanças importantes em sua vida quando seus pais decidem deixar a sua cidade natal, no estado de Minnesota, para viver em San Francisco. Dentro do cérebro de Riley, convivem várias emoções diferentes, como a Alegria, o Medo, a Raiva, o Nojinho e a Tristeza. A líder deles é Alegria, que se esforça bastante para fazer com que a vida de Riley seja sempre feliz. Entretanto, uma confusão na sala de controle faz com que ela e Tristeza sejam expelidas para fora do local. Agora, elas precisam percorrer as várias ilhas existentes nos pensamentos de Riley para que possam retornar à sala de controle - e, enquanto isto não acontece, a vida da garota muda radicalmente. ',
            '2015-6-17',
            'http://img2.timeinc.net/people/i/2015/news/150316/inside-out-435.jpg',
            'http://blogs-images.forbes.com/markhughes/files/2015/06/INSIDE-OUT-8-1940x1092.jpg',
            now()
        ),
        (
            'Pixels(2015)',
            5.7,
            'A humanidade sempre buscou vida fora da Terra e, em busca de algum contato, enviou imagens e sons variados sobre a cultura terrestre nos mais diversos satélites já lançados no universo. Um dia, um deles foi encontrado. Disposta a conquistar o planeta, a raça alienígena resolveu criar monstros digitais inspirados em videogames clássicos dos anos 1980. Para combatê-los, a única alternativa é chamar especialistas nos jogos: Sam Brenner (Adam Sandler), Eddie Plant (Peter Dinklage), Ludlow Lamonsoff (Josh Gad) e a tenente-coronel Violet Van Patten (Michelle Monaghan). ',
            '2015-7-25',
            'https://upload.wikimedia.org/wikipedia/en/f/f0/PixelsOfficialPoster.jpg?1447852349023',
            'http://icdn1.digitaltrends.com/image/pixels-movie-3-1500x844.jpg',
            now()
        ),
        (
            'Sharknado',
            8.7,
            'Uma grande tornado surge no litoral da Califórnia... O fenômeno natural começa no mar e interfere na vida de milhares de tubarões, que são sugados do oceano e arremessados por toda Los Angeles. Acaba sobrando para Fin (Ian Ziering), sua esposa April (Tara Reid) e mais um grupo de amigos a missão de enfrentar os animais e impedir que o tornado causa ainda mais estragos.',
            '2013-7-11',
            'https://upload.wikimedia.org/wikipedia/en/9/93/Sharknado_poster.jpg?1447852607335',
            'http://rack.2.mshcdn.com/media/ZgkyMDEzLzA3LzEyL2E3L3NoYXJrbmFkb2F0LmQ4MjE3LmpwZwpwCXRodW1iCTEyMDB4NjI3IwplCWpwZw/a4be2f8e/d87/sharknado-attack.jpg',
            now()
        ),
        (
            'Os Vingadores',
            8.1,
            'Loki (Tom Hiddleston) retorna à Terra enviado pelos chitauri, uma raça alienígena que pretende dominar os humanos. Com a promessa de que será o soberano do planeta, ele rouba o cubo cósmico dentro de instalações da S.H.I.E.L.D. e, com isso, adquire grandes poderes. Loki os usa para controlar o dr. Erik Selvig (Stellan Skarsgard) e Clint Barton/Gavião Arqueiro (Jeremy Renner), que passam a trabalhar para ele. No intuito de contê-los, Nick Fury (Samuel L. Jackson) convoca um grupo de pessoas com grandes habilidades, mas que jamais haviam trabalhado juntas: Tony Stark/Homem de Ferro (Robert Downey Jr.), Steve Rogers/Capitão América (Chris Evans), Thor (Chris Hemsworth), Bruce Banner/Hulk (Mark Ruffalo) e Natasha Romanoff/Viúva Negra (Scarlett Johansson). Só que, apesar do grande perigo que a Terra corre, não é tão simples assim conter o ego e os interesses de cada um deles para que possam agir em grupo.',
            '2012-4-23',
            'https://upload.wikimedia.org/wikipedia/pt/thumb/2/22/OsVingadores2.jpg/255px-OsVingadores2.jpg',
            'http://moviehole.net/img/avengerswallpaper.jpg',
            now()
        ),
        (
            'Tomorrowland',
            6.5,
            'Casey Newton (Britt Robertson) é uma adolescente com enorme curiosidade pela ciência. Um dia, ela encontra um pequeno broche que permite que se transporte automaticamente para uma realidade paralela chamada Tomorrowland, repleta de invenções futuristas visando o bem da humanidade. Ela logo busca um meio de chegar ao lugar e, no caminho, conta com a ajuda da misteriosa Athena (Raffey Cassidy) e de Frank Walker (George Clooney), que esteve em Tomorrowland quando garoto mas hoje leva uma vida amargurada. ',
            '2015-6-4',
            'https://upload.wikimedia.org/wikipedia/en/8/80/Tomorrowland_poster.jpg?1447853960082',
            'http://i.kinja-img.com/gawker-media/image/upload/iiwuqsqudreabkdqln3u.jpg',
            now()
        ),
        (
            'Interestelar',
            8.7,
            'Após ver a Terra consumindo boa parte de suas reservas naturais, um grupo de astronautas recebe a missão de verificar possíveis planetas para receberem a população mundial, possibilitando a continuação da espécie. Cooper (Matthew McConaughey) é chamado para liderar o grupo e aceita a missão sabendo que pode nunca mais ver os filhos. Ao lado de Brand (Anne Hathaway), Jenkins (Marlon Sanders) e Doyle (Wes Bentley), ele seguirá em busca de uma nova casa. Com o passar dos anos, sua filha Murph (Mackenzie Foy e Jessica Chastain) investirá numa própria jornada para também tentar salvar a população do planeta.',
            '2014-11-7',
            'https://upload.wikimedia.org/wikipedia/en/b/bc/Interstellar_film_poster.jpg?1447854066624',
            'http://www.interstellar-movie.com/images/interstellar-movie-chris-nolan.png',
            now()
        ),
        (
            'EX_MACHINA',
            7.7,
            'Caleb (Domhnall Gleeson), um jovem programador de computadores, ganha um concurso na empresa onde trabalha para passar uma semana na casa de Nathan Bateman (Oscar Isaac), o brilhante e recluso presidente da companhia. Após sua chegada, Caleb percebe que foi o escolhido para participar de um teste com a última criação de Nathan: Ava (Alicia Vikander), uma robô com inteligência artificial. Mas essa criatura se apresenta sofisticada e sedutora de uma forma que inguém poderia prever, complicando a situação ao ponto que Caleb não sabe mais em quem confiar.',
            '2015-1-15',
            'http://t3.gstatic.com/images?q=tbn:ANd9GcQe8L-1PTMlUf-si2Oy6BTd9ZtbWH7BSRSF5k5JGNATxOHzyIdg',
            'https://jaysanalysis.files.wordpress.com/2015/04/maxresdefault.jpg',
            now()
        ),
        (
            'A Entrevista',
            6.7,
            'Cansados de fazerem entrevistas apenas com celebridades, um famoso apresentador de um popular programa de televisão (James Franco) e seu produtor (Seth Rogen) decidem buscar uma linha mais série de jornalismo. Eles conseguem marcar uma entrevista com o ditador Kim Jong-un, da Coreia do Norte. Sabendo disso, a CIA decidem convocar os dois sujeitos para um plano para assassinar o ditador. A coisa só complica um pouco quando o apresentador começa a se identificar com os dramas vividos pelo governante.',
            '2014-12-24',
            'https://upload.wikimedia.org/wikipedia/en/2/27/The_Interview_2014_poster.jpg?1447854455064',
            'http://d1oi7t5trwfj5d.cloudfront.net/c4/eb/6e56e24c48ee90928e7c49c5c337/interview.jpg',
            now()
        ),
        (
            'San Andreas (2015)',
            6.2,
            'Um terremoto atinge a Califórnia e faz com que Ray (Dwayne Johnson), um bombeiro especializado em restates com helicópteros, tenha que percorrer o estado ao lado da ex-esposa (Carla Gugino) para resgatar a sua filha Blake (Alexandra Daddario), que tenha sobreviver em São Francisco com a ajuda de dois jovens irmãos.',
            '2015-5-29',
            'https://upload.wikimedia.org/wikipedia/en/3/38/San_Andreas_poster.jpg?1447854674466',
            'http://static3.businessinsider.com/image/556dc9cf69bedd1754b23cfc-1200-600/san-andreas-3.png',
            now()
        ),
        (
            'Mad Max: Estrada da Fúria',
            9.1,
            'Após ser capturado por Immortan Joe, um guerreiro das estradas chamado Max (Tom Hardy) se vê no meio de uma guerra mortal, iniciada pela Imperatriz Furiosa (Charlize Theron) na tentativa se salvar um grupo de garotas. Também tentanto fugir, Max aceita ajudar Furiosa em sua luta contra Joe e se vê dividido entre mais uma vez seguir sozinho seu caminho ou ficar com o grupo.',
            '2015-5-7',
            'https://upload.wikimedia.org/wikipedia/pt/2/23/Max_Mad_Fury_Road_Newest_Poster.jpg?1447854889273',
            'http://www.movierulz25.com/wp-content/uploads/2015/08/high-octane-mad-max-fury-road-supercut-497095.jpg',
            now()
        )
        ,
        (
            'Fant4stico',
            3.3,
            'Quatro adolescentes são conhecidos pela inteligência e pelas dificuldades de inserção social. Juntos, são enviados a uma missão perigosa em uma dimensão alternativa. Quando os planos falham, eles retornam à Terra com sérias alterações corporais. Munidos desses poderes especiais, eles se tornam o Senhor Fantástico (Miles Teller), a Mulher Invisível (Kate Mara), o Tocha Humana (Michael B. Jordan) e o Coisa (Jamie Bell). O grupo se une para proteger a humanidade do ataque do Doutor Destino (Toby Kebbell).',
            '2015-8-7',
            'https://upload.wikimedia.org/wikipedia/en/f/f4/Fantastic_Four_2015_poster.jpg?1447854993712',
            'http://static.comicvine.com/uploads/original/11120/111204578/4853385-f4-promo-art.jpg',
            now()
        )
        ,
        (
            'Zumbilândia',
            8.4,
            'A população mundial foi dizimada devido a um vírus, variante do mal da vaca louca, que faz com que as pessoas se transformem em zumbis. Poucos são os humanos não infectados, entre eles Columbus (Jesse Eisenberg). Ele é um estudante da Univeridade do Texas, que deseja voltar para sua cidade natal na esperança de encontrar seus pais ainda vivos. Cheio de fobias, o maior medo de Columbus não são os zumbis, mas os palhaços. No caminho ele encontra Tallahassee (Woody Harrelson), que está indo para a Flórida com o objetivo de aniquilar o maior número possível de zumbis. Columbus pega uma carona com ele. Ao parar em uma mercearia, a dupla enfrenta três zumbis e encontra duas garotas, Wichita (Emma Stone) e sua irmã caçula Little Rock (Abigail Breslin). Só que Little Rock aparenta ter sido mordida por um zumbi, o que divide o grupo sobre o que fazer.',
            '2009-10-2',
            'https://upload.wikimedia.org/wikipedia/en/a/a3/Zombieland-poster.jpg?1447855210215',
            'http://thezombieapocalypseblog.com/wp-content/uploads/2015/02/zombieland-zombieland-11064555-1600-1200.jpg',
            now()
        ),
        (
            'Ela',
            8.6,
            'Theodore (Joaquin Phoenix) é um escritor solitário, que acaba de comprar um novo sistema operacional para seu computador. Para a sua surpresa, ele acaba se apaixonando pela voz deste programa informático, dando início a uma relação amorosa entre ambos. Esta história de amor incomum explora a relação entre o homem contemporâneo e a tecnologia.',
            '2013-12-18',
            'https://upload.wikimedia.org/wikipedia/en/4/44/Her2013Poster.jpg?1447855303413',
            'http://d1oi7t5trwfj5d.cloudfront.net/34/5e/e561d6e34683a65d357d83f7ce41/her.jpg',
            now()
        ),
        (
            'The Lego Movie',
            8.5,
            'Emmet (Chris Pratt) é um Lego comum, até o dia em que é confundido com o Master Builder, o grande criador deste mundo de brinquedo, por ter encontrado a famosa peça de resistência. Este peça, procurada por todos há séculos, seria capaz de desarmar uma poderosa máquina criada pelo presidente do país, o perverso Sr. Negócios, que pretende colar todas as peças e impedir as mudanças no sistema. Mesmo sem ter grandes habilidades como criador, Emmet gosta de ser considerado um Lego especial, e faz de tudo para merecer a confiança de seus amigos, que incluem a rebelde Mega Estilo, o sábio Vitrúvius, e o gato-unicórnio UniKitty.',
            '2014-2-6',
            'https://upload.wikimedia.org/wikipedia/pt/4/40/The_Lego_Movie.jpg?1447878359613',
            'http://unothegateway.com/wp-content/uploads/2015/07/lego-movie.jpg',
            now()
        ),
        (
            'The Dark Knight',
            9,
            'Após dois anos desde o surgimento do Batman (Christian Bale), os criminosos de Gotham City têm muito o que temer. Com a ajuda do tenente James Gordon (Gary Oldman) e do promotor público Harvey Dent (Aaron Eckhart), Batman luta contra o crime organizado. Acuados com o combate, os chefes do crime aceitam a proposta feita pelo Coringa (Heath Ledger) e o contratam para combater o Homem-Morcego.',
            '2008-7-18',
            'https://upload.wikimedia.org/wikipedia/en/8/8a/Dark_Knight.jpg?1447878496153',
            'http://i.kinja-img.com/gawker-media/image/upload/bdax36su2nbecapijz3l.jpg',
            now()
        ),
        (
            'Homem de Ferro',
            8.4,
            'Tony Stark (Robert Downey Jr.) é um industrial bilionário, que também é um brilhante inventor. Ao ser sequestrado ele é obrigado por terroristas a construir uma arma devastadora mas, ao invés disto, constrói uma armadura de alta tecnologia que permite que fuja de seu cativeiro. A partir de então ele passa a usá-la para combater o crime, sob o alter-ego do Homem de Ferro.',
            '2008-4-30',
            'https://upload.wikimedia.org/wikipedia/en/7/70/Ironmanposter.JPG?1447878695781',
            'http://all4desktop.com/data_images/original/4194862-iron-man-2-movie-still-normal.jpg',
            now()
        ),
        (
            'Transformers',
            7.1,
            'Durante anos os Autobots e os Decepticons, duas raças alienígenas robóticas, duelaram em Cybertron, seu planeta natal. Esta guerra fez com que o planeta fosse destruído, resultando que os robôs se espalhassem pelo universo. Megatron (Hugo Weaving), o líder dos Decepticons, vem à Terra em busca da Allspark, um cubo de Cybertron que possibilita que qualquer aparelho eletrônico seja transformado num robô com inteligência própria. Megatron encontra a Allspark, mas fica congelado no Ártico. Décadas depois outros Decepticons chegam à Terra, numa tentativa de encontrar a Allspark. Seu surgimento logo alerta os principais países, em especial os Estados Unidos, que tem uma base militar no Qatar atacada. Enquanto o Secretário de Defesa John Keller (Jon Voight) tenta descobrir o que está havendo, reunindo todas as forças e informações possíveis, o jovem Sam Witwicky (Shia LaBeouf) tem preocupações mais simples: conseguir uma boa nota no colégio, o que lhe garantirá seu 1º carro. Mas o que ele não esperava era que o veículo escolhido, um Camaro antigo, tinha vida própria.',
            '2007-7-18',
            'https://upload.wikimedia.org/wikipedia/en/6/66/Transformers07.jpg?1447878816849',
            'http://www.flickeringmyth.com/wp-content/uploads/2015/05/transformers-3-161182-1-transformers-5-confirmed-for-2016-release.jpeg',
            now()
        ),
        (
            'The Room',
            3.5,
            'Johnny (Tommy Wiseau) é um banqueiro bem-sucedido que vive em São Francisco com sua noiva Lisa (Juliette Danielle). Entediada com a vida que leva, Lisa decide seduzir o melhor amigo de seu companheiro, Mark (Greg Sestero). Em consequência, a vida de todos mudará. ',
            '2003-6-27',
            'https://upload.wikimedia.org/wikipedia/en/e/e1/TheRoomMovie.jpg?1447879124505',
            'http://chathamkenthospice.com/wp-content/uploads/2014/03/pretty-flower-pictures-roses.jpg',
            now()
        ),
        (
            'Inception',
            8.7,
            'Em um mundo onde é possível entrar na mente humana, Cobb (Leonardo DiCaprio) está entre os melhores na arte de roubar segredos valiosos do inconsciente, durante o estado de sono. Além disto ele é um fugitivo, pois está impedido de retornar aos Estados Unidos devido à morte de Mal (Marion Cotillard). Desesperado para rever seus filhos, Cobb aceita a ousada missão proposta por Saito (Ken Watanabe), um empresário japonês: entrar na mente de Richard Fischer (Cillian Murphy), o herdeiro de um império econômico, e plantar a ideia de desmembrá-lo. Para realizar este feito ele conta com a ajuda do parceiro Arthur (Joseph Gordon-Levitt), a inexperiente arquiteta de sonhos Ariadne (Ellen Page) e Eames (Tom Hardy), que consegue se disfarçar de forma precisa no mundo dos sonhos.',
            '2010-8-6',
            'https://upload.wikimedia.org/wikipedia/en/7/7f/Inception_ver3.jpg?1447879403590',
            'http://theartmad.com/wp-content/uploads/2015/08/Inception-Totem-Wallpaper-1.jpg',
            now()
        ),
        (
            'Rocky',
            8.1,
            'Rocky Balboa (Sylvester Stallone), um lutador de boxe medíocre que trabalha como "cobrador" de um agiota, tem a chance de enfrentar Apollo Creed (Carl Weathers), o campeão mundial dos pesos-pesados, que teve a idéia de dar oportunidade a um desconhecido como um golpe publicitário. Mas Rocky decide treinar de modo intensivo, sonhando apenas em terminar a luta sem ter sido nocauteado pelo campeão.',
            '1976-12-3',
            'https://upload.wikimedia.org/wikipedia/en/1/18/Rocky_poster.jpg?1447879530367',
            'http://images6.alphacoders.com/426/426805.jpg',
            now()
        ),
        (
            '2012',
            5.8,
            'Em 2008, o presidente americano (Danny Glover) convoca uma reunião de emergência com as principais potências para conversar sobre um grande perigo para a humanidade. Os anos passam e, com a proximidade de 2012, as autoridades decidem que não é mais possível conter o perigo eminente que pode significar o fim do mundo. Com isso, colocam em prática o plano iniciado anos atrás, sob o comando dos cientistas Adrian Helmsley (Chiwetel Ejiofor) e Carl Anheuser (Oliver Platt). Enquanto isso, o escritor Jackson Curtis (John Cusack) leva sua vida de marido separado, pai de dois filhos, como motorista de limusine e tendo que aturar as reclamações da ex esposa (Amanda Peet). Ao levar os filhos para passear, ele descobre os primeiros sintomas da destruição do planeta.',
            '2009-11-13',
            'https://upload.wikimedia.org/wikipedia/en/d/dd/2012_Poster.jpg?1447879694355',
            'http://www.everythingaction.com/wp-content/uploads/2011/01/2012_la_limo_058_110_pubstill.jpg',
            now()
        )
