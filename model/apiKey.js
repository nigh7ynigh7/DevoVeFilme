var util = require('util');

//variaveis essenciais para a entidade e para retornar tudo.
var entity = null;
var func = {};

var allow = function(key){
    return entity.ApiKey.find({apikey : key}).then(function(a){
        if(a){
            return true;
        }
        return false;
    }, function(){
        return false;
    });
};
var create = function(){
    return entity.ApiKey.create();
};

func.allow = allow;
func.create = create;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return func;
    } else {
        throw new Error('Erro no módulo /model/apiKey.js, variável \'ent\' está nula. Ent deve ser o objeto retornado do modulo /sequelize/');
    }
};
