var util = require('util');

//variaveis essenciais para a entidade e para retornar tudo.
var entity = null;
var func = {};

var all = function(){
    return entity.Filme.findAll();
};
var find = function(name){
    return entity.Filme.findAll({
        where : {filme : { $iLike : "%" + name + "%" }}
    });
};
var recent = function(){
    return entity.Filme.findAll({limit : 20, order: 'lancamento DESC'});
};
var findOne = function(name){
    return entity.Filme.find({where:{filme:name}});
};
var create = function(name, rating, desc, lanc, posterimg, bgimg){
    if(!name || !rating || !desc){
        return new Error('Você deve informar o nome, nota, e descricao');
    }
    return entity.Filme.create({
        filme : name,
        rating : rating,
        descricao : desc,
        LinkPosterImg : posterimg,
        LinkBackgroundImg : bgimg,
        lancamento : lanc});
};
var destroy = function(id){
    return entity.Filme.destroy({where:{id:id}});
};

func.all = all;
func.find = find;
func.recent = recent;
func.findOne = findOne;
func.create = create;
func.destroy = destroy;

module.exports = function(ent){
    if (ent) {
        entity = ent;
        return func;
    } else {
        throw new Error('Erro no módulo /model/filme.js, variável \'ent\' está nula. Ent deve ser o objeto retornado do modulo /sequelize/');
    }
};
