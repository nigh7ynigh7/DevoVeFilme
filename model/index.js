module.exports = function(sequelize){
    var model = {};
    model.Filme = require('./filme')(sequelize);
    model.API = require('./apiKey')(sequelize);
    return model;
};
